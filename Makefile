# Default values for arguments
ALPINE_VERSION ?= 3
WAITFORIT_VERSION ?= latest

# Name of the Docker image
IMAGE_NAME := devthefuture/wait-for-it

# Combine the versions to create a tag
IMAGE_TAG := $(WAITFORIT_VERSION)-alpine-$(ALPINE_VERSION)

# Docker build command
build:
	docker build --build-arg ALPINE_VERSION=$(ALPINE_VERSION) \
		--progress=plain \
		-t $(IMAGE_NAME):$(IMAGE_TAG) \
		-t $(IMAGE_NAME):$(WAITFORIT_VERSION) .

# Docker push command
push:
	docker push $(IMAGE_NAME):$(WAITFORIT_VERSION)
	docker push $(IMAGE_NAME):$(IMAGE_TAG)

# Default target
all: build push

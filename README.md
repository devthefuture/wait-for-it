# wait-for-it container
Use [wait-for-it.sh](https://github.com/vishnubob/wait-for-it) in rootless container.

## Example usage
```sh
docker run devthefuture/wait-for-it codeberg.org:80 -- echo "codeberg is up"
```

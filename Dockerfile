ARG ALPINE_VERSION=3
FROM alpine:$ALPINE_VERSION
RUN apk add --update --no-cache netcat-openbsd bash
USER 1000
COPY wait-for-it.sh /
ENTRYPOINT ["/wait-for-it.sh"]